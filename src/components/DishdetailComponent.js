import React, { Component } from "react";
import {Card, CardImg, CardImgOverlay, CardText, CardBody, CardTitle, Breadcrumb, BreadcrumbItem, Col, Row, Button, Modal, ModalHeader, ModalBody,
    Form, FormGroup, Input, Label} from 'reactstrap';
import { Link } from 'react-router-dom';
import { Control, LocalForm, Errors } from 'react-redux-form';
import { Loading } from './LoadingComponent';
 

const maxLength = (len) => (val) => !(val) || (val.length <= len);
const minLength = (len) => (val) => val && (val.length >= len);

function RenderDish({dish}) {
    return(
        <div>                
            <div>
                <Card>                                   
                    <CardImg width="100%" src = {dish.image} alt = {dish.name}></CardImg>     
                    <CardBody>
                        <CardTitle>{dish.name}</CardTitle> 
                        <CardText>{dish.description }</CardText>
                    </CardBody>
                </Card>                        
            </div>           
            <div>               
               
            </div>           
        </div>  
    );
}

function RenderComments({comments, addComment, dishId}){
    console.log(comments)  
    if (comments != null) {

        let list = comments.map((comments)=>{

            return(
                <li key={comments.id} >
                    <div>
                        <p>{comments.comment}</p>
                        <p>--{comments.author},
                        {new Intl.DateTimeFormat('en-US', { year: 'numeric', month: 'short', day: '2-digit'}).format(new Date(Date.parse(comments.date)))}</p>
                    </div>
                </li>

            )
        })

        return(
                <div>
                    <h4>Comments</h4>
                    <ul className="list-unstyled">
                        {list}
                    </ul>  
                    <CommentForm dishId={dishId} addComment = {addComment}/>                
                </div>
        )
    }
    else{
        return(
            <div></div>
        )
    }
}

class CommentForm extends Component {

    constructor(props) {
        super(props);  

        this.state = {      
          dish: props.dish,    
          comments: props.comments,
          isModalOpen: false,
          isNavOpen: false
        }; 

        this.toggleModal = this.toggleModal.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

      }
      
      toggleModal() {
        this.setState({
          isModalOpen: !this.state.isModalOpen
        });
      }

      handleSubmit(values){
        this.toggleModal();
        {/*console.log('Current State is:' + JSON.stringify(values));
        alert('Current State is:' + JSON.stringify(values));*/}
        this.props.addComment(this.props.dishId, values.rating, values.author, values.comment);
      }

      render() {
          return(
            <di>
            <Button onClick={this.toggleModal} type="submit" color="primary">
                Submit Comment
            </Button>
            <Modal isOpen={this.state.isModalOpen} toggle={this.toggleModal}>
                                <ModalHeader toggle={this.toggleModal}>Submit Comment</ModalHeader>
                                <ModalBody>
                                    <LocalForm onSubmit={(values) => this.handleSubmit(values)}>
                                        <Col md={10}>
                                            <Row className="form-group">  
                                            <Label htmlFor="rating" md={6}>Rating</Label>                                            
                                            <Control.select model=".rating" name="rating"
                                                className="form-control">
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                                <option>4</option>
                                                <option>5</option>
                                                <option>6</option>
                                                <option>7</option>
                                                <option>8</option>
                                            </Control.select>
                                            </Row>
                                        </Col>
                                        <Col md={10}>
                                            <Row className="form-group">  
                                            <Label htmlFor="yourname" md={6}>Your Name</Label>
                                            <Control.text model=".yourname" id="yourname" name="yourname"
                                                placeholder="Your Name"
                                                className="form-control"
                                                validators={{
                                                    minLength: minLength(3), maxLength: maxLength(15)
                                                }}                                              
                                            />
                                            <Errors
                                                    className="text-danger"
                                                    model=".yourname"
                                                    show="touched"
                                                    messages={{                                                        
                                                        minLength: 'Must be greater than 2 characters',
                                                        maxLength: 'Must be 15 characters or less'
                                                    }}
                                                />
                                            </Row>
                                        </Col>
                                        <Col md={10}>
                                            <Row className="form-group">  
                                            <Label htmlFor="comment" md={6}>Comment</Label>
                                            <Control.textarea model=".comment" id="comment" name="comment"
                                                placeholder="Comment"
                                                rows="12"
                                                className="form-control"                                              
                                            />
                                            </Row>
                                        </Col>
                                        <Col md={10}>
                                        <Row className="form-group">                                          
                                                <Button type="submit" color="primary">
                                                Submit
                                                </Button>                                          
                                        </Row>
                                        </Col>
                                    </LocalForm>                      
                                </ModalBody>
                            </Modal>
            </di>
          );
      }
}

const DishDetail = (props) => {     
    if (props.isLoading){
        return(
            <div className="container">
                <di>
                    <Loading />
                </di>
            </div>
        );
    }
    else if (props.errMess){
        return(
            <div className="container">
                <di>
                    <h4>{props.errMess}</h4>
                </di>
            </div>
        );
    }
    else if (props.dish != null) 
        return (
            <div className="container">
                <div className="row">
                    <Breadcrumb>
                        <BreadcrumbItem><Link to="/menu">Menu</Link></BreadcrumbItem>
                        <BreadcrumbItem active>{props.dish.name}</BreadcrumbItem>
                    </Breadcrumb>
                    <div className="col-12">
                        <h3>{props.dish.name}</h3>
                        <hr />
                    </div>                
                </div>
                <div className="row">
                    <div className="col-12 col-md-5 m-1">
                        <RenderDish dish={props.dish} />
                    </div>
                    <div className="col-12 col-md-5 m-1">
                        <RenderComments comments={props.comments} 
                            addComment = {props.addComment}
                            dishId={props.dish.id}
                        />
                    </div>
                </div>
            </div>
        );    
    else
        return(
            <div>

            </div>
        );

     
}



export default DishDetail;